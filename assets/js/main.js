$(function() {
	var albums = [],
		perPage = 2,
		infoTable = $('.info-table').find('tbody');

	$.ajax({
		url: "assets/js/data.json",
		dataType: "json",
		success: function( data, textStatus, jqXHR ) {
			albums = generatePages(data);
			renderAlbumData(albums[0]);
			pagination(albums);
		}
	});

	$('body').on('click', '.page-item', function(event) {
		var page = $(this).data('page');
		$('.page-item.active').removeClass('active');
		$(this).addClass('active');

		renderAlbumData(albums[page]);
	});

	$('body').on('click', '.glyphicon-trash', function(){
		var r = confirm("Deseja remover o album da lista?");
		if (r == true) {
			$(this).parents('tr').remove();
		}
	});

	function generatePages(data) {
		var tempArr = []
		for (i=0,j=data.album.length; i<j; i+=perPage) {
			tempArr.push(data.album.slice(i,i+perPage));
		}

		return tempArr;
	}

	function renderAlbumData(data) {
		infoTable.html('');
		for (let index = 0; index < data.length; index++) {
			var item = templateAlbum(data[index]);
			infoTable.append(item);
		}
	}

	function pagination(album) {
		var pagination = $('.pagination'),
			active = '';
		$(album).each(function(index, el){
			var active = index == 0 ? 'active' : '';
			pagination.append('<li data-page="'+index+'" class="page-item '+active+'"><a class="page-link" href="javascript:;">'+(index+1)+'</a></li>');
		})
	}
});

function templateAlbum(album){
	// Validação para caso um dos itens esperado pela API não for retornado, já tratamos ele aqui para que o usuario final não veja "undefined" escrito na tela.
	// Um dos albuns do JSON não esta retornando o campo "realease" para mostrar a validação funcionando.
	var cover 		= album.hasOwnProperty('cover') 	? album.cover 		: 'Informação Indisponivel';
	var title 		= album.hasOwnProperty('title') 	? album.title 		: 'Informação Indisponivel';
	var artist 		= album.hasOwnProperty('artist') 	? album.artist 		: 'Informação Indisponivel';
	var duration 	= album.hasOwnProperty('duration') 	? album.duration 	: 'Informação Indisponivel';
	var songs 		= album.hasOwnProperty('songs') 	? album.songs 		: 'Informação Indisponivel';
	var release 	= album.hasOwnProperty('release') 	? album.release 	: 'Informação Indisponivel';

	return '<tr><td><img src="'+cover+'" class="img-thumbnail thumb rounded mx-auto d-block" /></td><td>'+title+'</td><td>'+artist+'</td><td>'+duration+'</td><td>'+songs.length+'</td><td>'+release+'<i class="glyphicon glyphicon-trash"></i></td></tr>';
}